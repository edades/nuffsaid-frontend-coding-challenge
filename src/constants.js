export const ERROR_TYPE = 0
export const WARNING_TYPE = 1
export const INFO_TYPE = 2
export const ERROR_COLOR = '#F56236'
export const WARNING_COLOR = '#FCE788'
export const INFO_COLOR = '#88FCA3'
