import React, { useState, useEffect } from 'react';
import generateMessage, { Message } from './Api';
import Container from '@mui/material/Container';
import Content from './Components/Content';
import Snackbar from '@mui/material/Snackbar';
import { ERROR_TYPE } from './constants'

const App: React.FC<{}> = () => {
  const [messages, setMessages] = useState<Message[]>([]);
  const [isRunning, setIsRunning] = useState(true);
  const [showError, setShowError] = useState(false)

  const clearMessage = (id: string) => {
    if (id !== 'all') {
      const newMessages = messages?.filter?.(msg => msg.id !== id)
      setMessages(newMessages)
    } else {
      setMessages([])
    }
  }

  useEffect(() => {
    if(isRunning) {
      const cleanUp = generateMessage((message: Message) => {
        if (message.priority === ERROR_TYPE) {
          setShowError(true)
        }
        setMessages(oldMessages => [...oldMessages, message]);
      });
      return cleanUp;
    }
  }, [setMessages, isRunning]);

  return (
    <Container maxWidth="lg">
      <Snackbar
        open={showError}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        autoHideDuration={2000}
        onClose={() => setShowError(false)}
        message="There is an Error"
      />
      <Content messages={messages} clearMessage={clearMessage} toggle={setIsRunning} isRunning={isRunning} />
    </Container>
  );
}

export default App;
