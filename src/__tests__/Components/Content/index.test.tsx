import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Content from '../../../Components/Content';

const mock_messages = [
  {
    id: '9/23/2021, 5:59:11 PM',
    createDate: '9/23/2021, 5:59:11 PM',
    message: 'text 1',
    priority: 0,
  },
  {
    id: '9/23/2021, 5:59:12 PM',
    createDate: '9/23/2021, 5:59:12 PM',
    message: 'text 2',
    priority: 0,
  },
  {
    id: '9/23/2021, 5:59:13 PM',
    createDate: '9/23/2021, 5:59:13 PM',
    message: 'text 3',
    priority: 1,
  },
  {
    id: '9/23/2021, 5:59:14 PM',
    createDate: '9/23/2021, 5:59:14 PM',
    message: 'text 4',
    priority: 1,
  },
  {
    id: '9/23/2021, 5:59:15 PM',
    createDate: '9/23/2021, 5:59:15 PM',
    message: 'text 5',
    priority: 2,
  }
]

it('Content component render correctly', () => {
  const ContentComponent = render(
    <Content
      messages={mock_messages}
      clearMessage={() => {}}
      toggle={() => {}}
      isRunning={true} />,
    );
  const btnRunning = ContentComponent.container.querySelector('#btnRunning');
  const msgBoxes = ContentComponent.container.getElementsByClassName('msgbox');
  expect(ContentComponent).toBeTruthy();
  expect(btnRunning).toHaveTextContent('Stop');
  expect(msgBoxes.length).toBe(5);
});

it('Toggle Start/Stop button', () => {
  const handleToggleButton = jest.fn()
  const ContentComponent = render(
    <Content
      messages={mock_messages}
      clearMessage={() => {}}
      toggle={handleToggleButton}
      isRunning={true} />,
    );
  const btnRunning = ContentComponent.getByTestId('btnRunning');
  fireEvent.click(btnRunning)
  expect(handleToggleButton).toHaveBeenCalledTimes(1)
});
