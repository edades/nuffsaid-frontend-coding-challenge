import styled from 'styled-components'
import Card from '@mui/material/Card';

const StyledCard = styled(Card)`
  background-color: ${props => props.bg};
  padding: 10px;
  margin: 10px 0;
  display: flex;
  justify-content: space-between;
`

export {
  StyledCard,
}
