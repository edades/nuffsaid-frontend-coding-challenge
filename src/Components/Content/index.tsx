import React from 'react';
import Grid from '@mui/material/Grid';
import {
  ERROR_TYPE,
  WARNING_TYPE,
  INFO_TYPE,
  ERROR_COLOR,
  WARNING_COLOR,
  INFO_COLOR,
} from '../../constants';
import { Message } from '../../Api';
import { StyledCard } from './styled';

const getColor = (type: Number) => {
  const color = {
    bg: 'white',
  };
  switch (type) {
    case ERROR_TYPE:
      color.bg = ERROR_COLOR
      break;
    case WARNING_TYPE:
      color.bg = WARNING_COLOR
      break;
    case INFO_TYPE:
      color.bg = INFO_COLOR
      break;
    default:
      color.bg = 'white'
      break;
  }
  return color
}

const get_messages = (messages: Array<Message>, type: Number, clearMessage: Function) => {
  const sortedMessages = messages.sort((a, b) =>
    new Date(b.createDate).valueOf() - new Date(a.createDate).valueOf()
  );
  return sortedMessages?.map?.((msg: Message) => {
    const { bg } = getColor(type)
    return (
      <StyledCard bg={bg} key={msg?.id} className="msgbox">
        <p>{msg?.message}</p>
        <p key={msg?.id} onClick={() => clearMessage(msg?.id)}>clear</p>
      </StyledCard>
    )}
  )
}

const Content = (props: any) => {
  const { messages, clearMessage, toggle, isRunning } = props
  const e_messages = messages?.filter?.((m: Message) => m.priority === ERROR_TYPE)
  const w_messages = messages?.filter?.((m: Message) => m.priority === WARNING_TYPE)
  const i_messages = messages?.filter?.((m: Message) => m.priority === INFO_TYPE)
  return (
    <>
      <div style={{ height: 50, display: 'flex', justifyContent: 'center' }}>
        <button id="btnRunning" data-testid="btnRunning" onClick={() => toggle((oldState: Boolean) => !oldState)}>
          {isRunning ? 'Stop' : 'Start'}
        </button>
        <button id="btnClear" onClick={() => clearMessage('all')}>Clear</button>
      </div>
      <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
        <Grid item xs={12} sm={12} md={4} key={ERROR_TYPE}>
          <h3>Error Type 1</h3>
          <p>Count {e_messages.length}</p>
          {get_messages(e_messages, ERROR_TYPE, clearMessage)}
        </Grid>
        <Grid item xs={12} sm={12} md={4} key={WARNING_TYPE}>
          <h3>Warning Type 2</h3>
          <p>Count {w_messages.length}</p>
          {get_messages(w_messages, WARNING_TYPE, clearMessage)}
        </Grid>
        <Grid item xs={12} sm={12} md={4} key={INFO_TYPE}>
          <h3>Info Type 3</h3>
          <p>Count {i_messages.length}</p>
          {get_messages(i_messages, INFO_TYPE, clearMessage)}
        </Grid>
      </Grid>
    </>
  );
};

export default Content;
